Auto Demo Setup
===============

Run "demo.bat" for regular demo, run "demo-chills.bat" for fancy demo.

Manual Demo Setup
=================

1. Start server:

    python server.py
    
2. Start N-clients in different terminals (with optional fancy param)

    python client.py [fancy]
    
3. Start the beacon with the desired input file:

    python beacon.py [input.txt]
