
import zmq
import time
from helpers import addr
import sys
import os

def beacon(port, messages):
    context = zmq.Context()
    socket = context.socket(zmq.PUSH)
    socket.connect(addr(port))
    print("[Beacon]")  
    
    curr_time = 0
    
    for (at, msg) in messages:
      time.sleep((at-curr_time)/1000.0)
      curr_time = at
      
      socket.send(msg)

def from_file(filename):
    lines = open(filename).readlines()  
    tuples = map(lambda l:  ( int(l[:l.find(" ")]), l[l.find(" ") + 1:].replace('\\n', '\n')), lines)
    return tuples


def usage():
    print("Usage: beacon.py <inputfile.txt>")
    
if __name__ == '__main__':
    if len(sys.argv) != 2:
        usage()    

    elif len(sys.argv) == 2:
        messages = from_file(sys.argv[1])
        if sys.argv[1] == "chills.txt":
            os.startfile('chills.mp3')
        beacon(6000, messages)