import os

def addr(port):
    """Return local address string from a given port:
    i.e. tcp://127.0.0.1:1233
    """
    return "tcp://127.0.0.1:%s" % port

if os.name == 'nt':
    clear = 'cls'
else:
    clear = "clear"
