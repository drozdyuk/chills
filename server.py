
import zmq
import time
from helpers import addr

def server(port, in_port):
    context = zmq.Context()
    socket = context.socket(zmq.DEALER)
    socket.bind(addr(port))
    
    messages = context.socket(zmq.PULL)
    messages.bind(addr(in_port))
    
    print "[Server]"
    while(True):
        message = messages.recv()
        socket.send_multipart([b"", message])
        
server(5000, 6000)