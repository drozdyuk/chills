import zmq
import sys
from pyfiglet import Figlet
import os
import random
from helpers import addr, clear
    

def client(port, fancy=False):
    fonts = map(lambda f: f.strip(), open('fonts.txt').readlines())

    context = zmq.Context()
    socket = context.socket(zmq.ROUTER)
    socket.connect(addr(port))
    
    dirty = False
    print("[Client]")

    while(True):
        if socket.poll(20000): # quit after a few seconds if inactivity
            ident, _, msg = socket.recv_multipart()
            dirty = True
            
            if fancy:
                font = random.choice(fonts)  
                os.system(clear)
                f = Figlet(font=font)
                print f.renderText(msg)
            else:
                print msg
            
            
        else:
            if dirty:
                print("No messages received in a while. Quitting.")
                break


if __name__ == '__main__':
    if len(sys.argv) == 1:
        client(5000)

    elif len(sys.argv) == 2:
        os.system(clear)
        client(5000, fancy=True)